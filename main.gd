###################################
# author: mrzepisko
# project: asteroids-remake
# created: 2015-11-10 00:29
###################################

extends Node2D

const SPAWNER_SCENE = preload("res://spawnerClassic.xml");
const GUI_SCENE = preload("res://gui.xscn");
#members - pola wykorzystywane do mechaniki
var gameRunning = false;
var pause = false;
var dead = false; #flaga czy aktualnie martwy
var ship; #referencja do obiektu statku
var speedVector = Vector2(0.0, 0.0); #wektor predkosci, modyfikowany przez dodawanie gazu, ds/dt zmiana pozycji w czasie
var rotation = 0; #aktualny kat obrotu statku, na potrzeby wyliczania kierunku pociskow
var score = 0; #wynik
var lives; #ilosc zyc
var immune = false; #czy chroniony (np przy respawnie)
var bulletCounter = 0; #ilosc stworzonych instancji pociskow, kazdy ma dzieki temu unikatowa nazwe przy tworzeniu obiektu
var rockCounter = 0; #j/w tylko dla skal
var ufoCounter = 0;
var animCounter = 0;
var respawning = false; #czy trwa respawn
var level = 0;
#timery
var timer = 0; #czasomierz
var shootingTimer; #to nie pamietam :P
var respawnTimer; #czas nieaktywnosci podczas respawnu
var immunityTimer = 0; #czas niesmiertelnosci
var teleTimer;
var ufoSpawnTimer;

var camera;

#pomocnicze zmienne
var shooting = false; #czy aktualnie strzela
var gui_pause_select = 0;
var gui_confirm_exit = false;
var titleSceneHolder = preload("res://title.xml");
var titleScene;
var lib; #klasy
var globalScript;

var animations = preload("res://anims.xml").instance();
var animList = { 
	"DESTROY_ROCK":"destroy/default",
	"DESTROY_UFO":"destroy/default",
	"DESTROY_BULLET":"destroy/bullet",
	"EXPLOSION_R_BIG":"explosions/rBig",
	"EXPLOSION_M_BIG":"explosions/rMedium",
	"EXPLOSION_S_BIG":"explosions/rSmall",
	"EXPLOSION_SHIP":"explosions/ship",
	"EXPLOSION_UFO_BIG":"explosions/ufoL",
	"EXPLOSION_UFO_SMALL":"explosions/ufoS",
	}
var up_key_hold = false;
var down_key_hold = false;
var esc_key_hold = false;
var help_key_hold = false;
var voices_move;
var _spawner
var _sounds = preload("res://soundsClassic.xml");
var samplePlayer;
signal fire_anim(position, velocity, rotation, type);
signal fire_bullet(position, rotation, speedVector, owner);

func _enter_tree():
	lib = get_node("/root/classes"); #ustawiamy sobie zmienna lokalna dla wygody
	globalScript = get_node("/root/globals");
	globalScript.readSettings(); #ambitny odczyt z pliku ustawien
	
	_spawner = SPAWNER_SCENE.instance(); #spawner
	samplePlayer = SamplePlayer2D.new(); 
	samplePlayer.set_sample_library(_sounds);
	samplePlayer.set_polyphony(lib.MAX_VOICES); # obslugi obiekt do dzwiekow
	
	var gui = GUI_SCENE.instance(); #utworzenie nakladki z gui
	remove_child(get_node("camera/gui")); #to juz chyba nie potrzebne
	get_node("camera").add_child(gui); #dodanie nakladki do gui
	gui.set_pos(Vector2(-960, -540)); #i wysrodkowanie dla kamery
	
	camera = get_node("camera");
	camera.set_pos(lib.START_POS); #kamera na srodku
	
	globalScript.gameOver = false; #przy odpalaniu ustawiamy globalne gameOver na flase
	
	

#stad odpalamy animacje, tu do dopisania if
func launchAnimatedObject(position, velocity, rotation, type, particles = false):
	if type == null || !animList.has(type):
		return; #brak animacji
	var animName = animList[type]; #z dictionary id node z animacja
	
	#tu juz bez modyfikacji dodatkowych
	var obj;
	if !particles: #pobranie odpowiedniego typu do animacji particle/animPlayer
		obj = lib.Anim.new("anim" + str(animCounter), position, velocity, rotation, getAnimSprite(animName));
	else:
		obj = lib.AnimParticles.new("anim" + str(animCounter), position, velocity, rotation, getAnimSprite(animName));
	animCounter += 1; #na potrzeby nazywania obiektow
	add_child(obj); #dodanie animacji do sceny
func getAnimSprite(name):
	return animations.get_node(name); #pobranie ze sceny animacji odpowiednich nodeow z animacja
#_ready inicjalizacja mechaniki gry
func _ready():
#	var gl = get_node("/root/globals").randomRange(1, 1);
	
	lives = lib.START_LIVES; #ustawianie poczatkowych zyc
	shootingTimer = lib.SHOOTING_PERIOD + 1; #na potrzeby strzelania
	
	get_node("camera/gui/lblGameOver").hide(); #ukrywanie labeli z game over
	titleScene = titleSceneHolder.instance(); #gui z tytulem
	add_child(titleScene);
	
	init_timers(); #ustawiamy timery
	connect_events(); #podlaczenie eventow (dzwieki, animacje)
	
	#ustawianie referencji do obiektu statku
	ship = _spawner.get_player();
	add_child(ship);
	ship.add_to_group(lib.GRP_COLLISIONS);
	ship.hide(); #nadal jestesmy na ekranie startu
	
	add_child(samplePlayer); #dodanie elementu z dzwiekami
	
	get_node("camera/gui").is_set_as_toplevel(); #?? tu chyba mielismy gui na sama gore ustawic ale to nie ta metoda
	
	#odpalamy klateczki!
	set_process(true);

func _process(delta): #maly burdel sie z klawiszami zrobil, bo byl robiony na ostatnia chwile
	if gui_confirm_exit: #obsluga klawiszy przy potwierdzeniu wyjscia
		if Input.is_action_pressed("ui_select"): #wyjscie
			get_tree().quit();
		elif Input.is_action_pressed("ui_cancel"): #powrot do poprzedniego widoku
			get_node("camera/gui/confirm_exit").hide();
			gui_confirm_exit = false;
	elif !pause: #jezeli koniec pauzy
		timer(delta) #aktualizacja timerow (staaaary sposob zanim poznalem timery, musialem go nie zauwazyc przy poprawianiu)
		#obsluga sterowania
		check_input();
		
		#poruszanie skalami, pociskami itp
		move_objects(delta);
		
		if gameRunning: #rzeczy do zrobienia gdy trwa rozgrywka
			#ustawianie predkosci i obrotu
			ship.set_rot(rotation);
			ship.translate(speedVector * delta);
			#kolizje
			check_collisions();
			#odpornosc
			check_immunity(delta);
			#tele - jak statek wyleci poza pole gry teleportujemy go na przeciwlegla krawedz
			if ship.get_pos().x > lib.WIDTH:
				ship.set_pos(ship.get_pos() - Vector2(lib.WIDTH, 0.0));
			elif ship.get_pos().x < 0:
				ship.set_pos(ship.get_pos() + Vector2(lib.WIDTH, 0.0));
			if ship.get_pos().y < 0:
				ship.set_pos(ship.get_pos() + Vector2(0.0, lib.HEIGHT));
			elif ship.get_pos().y > lib.HEIGHT:
				ship.set_pos(ship.get_pos() - Vector2(0.0, lib.HEIGHT));
		else:
			if Input.is_action_pressed("ui_highscore"): #przejscie do highsccore
				get_node("/root/globals").setScene("res://gover.xscn");
			elif !dead && Input.is_action_pressed("ui_exit"): #menu wyjscia z gry
				get_node("camera/gui/confirm_exit").show();
				gui_confirm_exit = true;
				esc_key_hold = true;
	else: #obsluga menu pauzy
		if Input.is_action_pressed("ui_up"): #do gory
			if !up_key_hold:
				gui_pause_select = abs(gui_pause_select - 1) % 2; #matematyka <3
				focus_option();
				up_key_hold = true;
		elif up_key_hold: #flaga czy nadal wcisniety
			up_key_hold = false;
		if Input.is_action_pressed("ui_down"):#opcja w dol
			if !down_key_hold:
				gui_pause_select = (gui_pause_select + 1) % 2; #czyli zeby trzymajac strzalke w gore/dol moc skakac pomiedzy ostatnia a pierwsza opcja
				focus_option();
				down_key_hold = true;
		elif down_key_hold:
				down_key_hold = false;
		
		if Input.is_action_pressed("ui_exit"): #tu chyba mialo byc ukrywanie menu pauzy na esc
			pass
		elif Input.is_action_pressed("ui_accept"): #zatwierdzenie opcji
			if gui_pause_select == 0: #jezeli continue to ukrywamy menu pauzy
				pause = false;
				get_node("camera/gui/pause_menu").hide();
			elif gui_pause_select == 1: #jezeli quit game to potwierdzenie wyjscia
				gui_confirm_exit = true;
				get_node("camera/gui/confirm_exit").show();
	#aktualizacja gui
	update_UI(delta);
#end _process(); tu konczy sie funkcja renderowania kazdej klatki

func focus_option(): #wlaczanie shadera do animacji wybranej opcji
	var opt_cont = get_node("camera/gui/pause_menu/pause/options/continue");
	var opt_quit = get_node("camera/gui/pause_menu/pause/options/quit_game");
	if gui_pause_select == 0: #przypisanie do odpowiedniej opcji mrygania
		opt_cont.set_material(opt_quit.get_material());
		opt_quit.set_material(null);
	elif gui_pause_select == 1:
		opt_quit.set_material(opt_cont.get_material());
		opt_cont.set_material(null); #i usuwanie shadera z nieaktywnej

func respawn(delay = false): #funckja do respawnu
	if voices_move != null: #zabezpieczenie zanim byl init samplePlayera
		samplePlayer.stop_voice(voices_move); #stop dzwiekom jezeli sie nie rusza
	set_immunity(lib.RESPAWN_TIME); #chroniony przez RESPAWN_TIME;
	if delay: #jezeli respawn z opoznieniem (inny jak przy start game)
		ship.hide();
		set_immunity(lib.RESPAWN_TIME); #chroniony przez RESPAWN_TIME;
		respawnTimer.start();
		dead = true;
		up_key_hold = false;
		return;
	ship.show();
	ship.set_pos(lib.START_POS); #pozycja startowa
	speedVector = Vector2(0, 0); #zerowa predkosc
	rotation = 0; #obrot domyslny
	dead = false;

func update_UI(delta):
	get_node("camera/gui/lblFps").set_text(str(round(1.0/delta)) + "fps"); #wyswietlanie fpsow
	get_node("camera/gui/lblScore").set_text(str(score)); #wynik
#obsluga sterowania
func check_input():
	if gameRunning:
		#strzal - tylko jezeli zywy
		if !dead:
			if Input.is_action_pressed("ui_space"):
				if !shooting:
					fire()
					shooting = true;
				else:
					shooting = false;
					
		#przyspieszanie
			if Input.is_action_pressed("ui_up"): #ograniczamy od razu predkosc
				speedVector = limit_speed(lib.SPEED_FACTOR * speedVector -  lib.ACC_FACTOR * Vector2(sin(rotation), cos(rotation)).normalized());
				if !up_key_hold:
					ship.get_node("jet").set_emitting(true);
					voices_move = samplePlayer.play("move", true); #odpalenie dzwieku ruchu (loop)
				up_key_hold = true;
			elif up_key_hold:
				samplePlayer.stop_voice(voices_move);
				ship.get_node("jet").set_emitting(false); #uruchomienie particli z silnika
				up_key_hold = false;
		#obrot w lewo
			if Input.is_action_pressed("ui_left"):
				rotation = rotation + lib.ROTATION_SPEED;
		#obrot w prawo
			elif Input.is_action_pressed("ui_right"):
				rotation = rotation - lib.ROTATION_SPEED;
		#teleport
			if Input.is_action_pressed("ui_tele"):
				teleport();
		#na wszelki wypadek jakby mial skońnczyc sie zakres floata (po 2^infinite obrotach w prawo)
			if rotation > 180:
				rotation -= 180;
			if rotation < -180:
				rotation += 180;
		#pause menu
		if Input.is_action_pressed("ui_exit"):
			pause = true;
			get_node("camera/gui/pause_menu").show();
			up_key_hold = false;
			if voices_move != null:
				samplePlayer.stop_voice(voices_move);
				ship.get_node("jet").set_emitting(false);
			
	else: #gdy gra nie smiga -> ekran tytulowy wiec czekamy na start
		if Input.is_action_pressed("ui_accept") && !globalScript.gameOver:
			start_game();
			
		
	if Input.is_action_pressed("ui_help"):
		if !help_key_hold:
			get_node("camera/gui/help").show();
		help_key_hold = true;
	elif help_key_hold:
		get_node("camera/gui/help").hide();
		help_key_hold = false;

#poruszamy wszystkim co wyladowalo w grupie MOVING
func move_objects(delta):
	for obj in get_tree().get_nodes_in_group(lib.GRP_MOVING):
		obj.move(delta);
		
#sprawdzanie kolizji TODO, zmienilem teksture z prostokata na okrag, ale metoda zostala, latwiej bedzie policzyc polozenie pomiedzy srodkiem asteroidy a pociskiem
func check_collisions():
	#pobieramy wszystkie pociski i wszystkie asteroidy itp
	var aBullets = get_tree().get_nodes_in_group(lib.GRP_BULLETS);
	var aRocks = get_tree().get_nodes_in_group(lib.GRP_ROCKS);
	var aPredators = get_tree().get_nodes_in_group(lib.GRP_PREDATOR);
	var aUfos = get_tree().get_nodes_in_group(lib.GRP_UFO); #trzeba statek tez zrobic jako obiekt poruszajacy sie, te grupy beda podatne na skalki i bedzie ladniej
#	if aRocks.size() > 0: #jezeli nie ma asteroid albo pociskow to nie ma co sprawdzac => wydajnosc jest istotna bo karta graficzna nie da rady :P
#	powyzszy komentarz byl pisany pierwszego dnia i juz wiedzialem ze cos pojdzie nie tak :) linijka niewazna, bo nie tylko kolizje sa ze skalami
	for bullet in aBullets:
		var bullPos = bullet.get_pos();
		for rock in aRocks:
			var rockPos = rock.get_pos();
			var rockRadius = rock.get_width() / 2.0; #asteroidy sa okragle, wystarczy promien i odleglosc elementu od srodka do kolizji
			for ufo in aUfos: #kolizja ufo vs skala
				var ufoPos = ufo.get_pos();
				var ufoRadius = ufo.get_width() / 2.0;
				if ufoPos.distance_to(rockPos) < rockRadius + ufoRadius: #jezeli srodek ufo jest blizej od srodka skaly jak suma ich promieni
					samplePlayer.stop_voice(ufo.voice_id);
					samplePlayer.play("crash", samplePlayer.NEXT_VOICE); #dzwiek wybuchu!
					ufo.destroy();
					camera.start_shaking(0.05);
					spawn_ufo()
			if bullPos.distance_to(rockPos) < rockRadius: #kolizja pocisk vs skala, pocisk mozna uznac ze jest punktem
				bullet.destroy();
				if rock.size() < 2: #rozpadanie skal na dwie mniejsze
					create_rock_on_pos(rock.get_pos(), lib.ROCK_CHILDREN, rock.size());
				rock.destroy();
				camera.start_shaking(0.05);
				samplePlayer.play("rock_d", samplePlayer.NEXT_VOICE); #dzwiek wybuchu!
				if bullet.owner == ship.get_name():
					score += rock.score();
		for ufo in aUfos: #kolizja ufo vs bullet
			var ufoPos = ufo.get_pos();
			var ufoRadius = ufo.get_width() / 2.0;
			if bullPos.distance_to(ufoPos) < ufoRadius && bullet.owner != ufo.get_name(): #ufo moze zabic inne ufo, ale nie siebie!
				bullet.destroy();
				samplePlayer.stop_voice(ufo.voice_id);
				samplePlayer.play("crash", samplePlayer.NEXT_VOICE);
				ufo.destroy();
				spawn_ufo()
				camera.start_shaking(0.05);
				samplePlayer.play("hit", samplePlayer.NEXT_VOICE);
				if bullet.owner == ship.get_name(): #wlasne pociski nie trafiaja
					score += ufo.score();
			if lib.SHIP_WIDTH + ufoRadius >= ship.get_pos().distance_to(ufoPos):
				destroy_ship();
		if bullet.owner != ship.get_name() && bullPos.distance_to(ship.get_pos()) < lib.SHIP_WIDTH: #trafienie ufo w statek
			bullet.destroy();
			destroy_ship();
		if get_tree().get_nodes_in_group(lib.GRP_PREDATOR).size() == 0:
			next_level();
	if aPredators.size() > 0 && !immune: #czy sa jakies niebezpieczne przedmioty i czy nie jest odporny
		for rock in aRocks: #sprawdzamy kolizje statek vs kazda skalka
			var rockPos = rock.get_pos();
			var rockRadius = rock.get_width() / 2.0;
			if lib.SHIP_WIDTH + rockRadius >= ship.get_pos().distance_to(rockPos): #na podstawie promieni, jezeli sie przecinaja to kolizja leci
				destroy_ship();
#			var ship = Polygon2D.new();
#			if ship + rockRadius >= ship.get_pos().distance_to(rockPos): #na podstawie promieni, jezeli sie przecinaja to kolizja leci
#				destroy_ship();
func destroy_ship():
	camera.start_shaking(); #efekt do ruszania kamera przy trafieniu
	lives -= 0; #i odpalamy respawn z odpowiednimi danymi wczesniej
	dead = true;
	samplePlayer.play("crash", samplePlayer.NEXT_VOICE);
	emit_signal("fire_anim", ship.get_pos(), speedVector, rotation, "EXPLOSION_SHIP", true);
	respawn(true);
	var strLives = "";
	for i in range(0, lives): #i poprawiamy label z iloscia zycia
		strLives += "x";
	get_node("camera/gui/lblLives").set_text(strLives)
	if lives <= 0:
		game_over();
	
func set_immunity(time): #tarcza ochronna po respawnie
	immunityTimer = time;
	immune = true;
	ship.get_node("shield").show();
	
func check_immunity(delta): #sprawdzanie czy niesmiertelny
	if !immune: #niechroniony wiec nic nie robimy
		return;
	immunityTimer -= delta;
	if immunityTimer <= 0:
		immune = false;
		ship.get_node("shield").hide();
#timery
func timer(delta): #najstarszy sposob timerow, jak jeszcze nie znalem wbudowanych, o ktorym zapomnialem
	timer += delta
	shootingTimer += delta;
 
#strzelanie
func fire():
	#to mialo sens wczoraj, ale dziala wiec nie rozstrzasam (BULLET_AMMOUNT pociskow wystrzeliwanych co SHOOTING_PERIOD sekundy)
	if shootingTimer < lib.SHOOTING_PERIOD * lib.BULLET_AMMOUNT: #|| bullets.size() > BULLET_AMMOUNT: #bylo ograniczenie jeszcze na ilosc naraz, ale szybsze pociski niezauwazalnie krocej zyja
		return;
	samplePlayer.play("shot", samplePlayer.NEXT_VOICE);
	create_bullet(); #odpalamy pocisk
	camera.start_shaking(0.05);
		
#ograniczenie maksymalnej predkosci
func limit_speed(vector):
	if vector.length() > lib.MAX_SPEED: 
		return vector * (lib.MAX_SPEED / vector.length());
	else:
		return vector;

#tworzymy ammount skal o podanym rozmiarze (0 - najwieksza), wiec mozna potem zaimplementowac latwo wiecej jak 3 rozmiary
func create_rock(ammount, size):
	for i in range(0, ammount):
		var rock = _spawner.new_rock(size); #tworzymy obiekt typu Rock (ktory rownoczesnie jest Spritem, takim samym jak przeciagnietym recznie jako Create New Node, magiczne slowo POLIMORFIZM)
		add_child(rock); #dodawania stworzonego obiektu do aktualnej sceny
func create_rock_on_pos(position, ammount, size):
	for i in range(0, ammount):
		var rock = _spawner.new_rock(size + 1); #tworzymy obiekt typu Rock (ktory rownoczesnie jest Spritem, takim samym jak przeciagnietym recznie jako Create New Node, magiczne slowo POLIMORFIZM)
		add_child(rock); #dodawania stworzonego obiektu do aktualnej sceny
		rock.set_pos(position);

#identyczna sprawa jak ze skalami, tylko przy tworzeniu pocisku trzeba podac wiecej danych
func create_bullet():
		var bullet = _spawner.new_bullet(ship.get_pos(), rotation, speedVector, ship.get_name());
		add_child(bullet);
		shootingTimer = 0; #wystrzelono pocisk, timer leci od nowa
	
#co sie dzieje na poczatku
func start_game():
	gameRunning = true;
	respawn();
	ship.show();
	titleScene.hide();
	score = 0;
	level = 1;
	lives = lib.START_LIVES;
	remove_rocks();
#	create_rock(1, 2); #nowe skałlki TODO
	randomize();
	create_rock(rand_range(3,4), 0); #nowe skałlki
	
func next_level(): #ustawianie opcji na kolejny level
	var currentScore = score;
	var currentLives = lives;
	var currentLevel = level;
	var currentSpeed = speedVector;
	var currentPos = ship.get_pos();
	var currentRotation = rotation;
	start_game();
	score = currentScore;
	lives = currentLives;
	level = currentLevel + 1;
	speedVector = currentSpeed;
	ship.set_pos(currentPos);
	rotation = currentRotation;
	if score >= lib.UFO_MIN_SCORE:
		spawn_ufo();
		ufoSpawnTimer.start();
#co sie dzieje na koncu
func game_over():
	respawnTimer.stop();
	samplePlayer.stop_voice(voices_move); 
	globalScript.score = score;
	gameRunning = false;
	globalScript.gameOver = true;
	get_node("camera/gui/lblGameOver").show();
	ship.hide();
	get_node("/root/globals").changeSceneTimeout("res://gover.xscn", lib.SCENE_CHANGE_TIME);
func spawn_ufo(): #uruchomienie ufo
	randomize();
	var rSize = randi();
	var ufo = _spawner.new_ufo(rSize % 2 == 0);
	add_child(ufo);
	ufo.voice_id = samplePlayer.play("ufo", samplePlayer.NEXT_VOICE);

func recreate_rocks(): #przestawienie pozycji skalek
	for rock in get_tree().get_nodes_in_group(lib.GRP_ROCKS):
		rock.random();
func remove_rocks():
	for rock in get_tree().get_nodes_in_group(lib.GRP_ROCKS):
		globalScript.clearGroups(rock);
		rock.queue_free();
func init_timers(): #obsluga timerow
	teleTimer = Timer.new();
	teleTimer.set_autostart(false);
	teleTimer.set_one_shot(true);
	teleTimer.set_wait_time(lib.TELE_COOLDOWN);
	add_child(teleTimer);
	
	ufoSpawnTimer = Timer.new();
	ufoSpawnTimer.set_autostart(false);
	ufoSpawnTimer.set_one_shot(false);
	ufoSpawnTimer.set_wait_time(lib.UFO_SPAWN_DELAY);
	ufoSpawnTimer.connect("timeout", self, "spawn_ufo");
	add_child(ufoSpawnTimer);
	
	respawnTimer = Timer.new();
	respawnTimer.set_autostart(false);
	respawnTimer.set_one_shot(true);
	respawnTimer.set_wait_time(lib.RESPAWN_TIME);
	respawnTimer.connect("timeout", self, "respawn");
	add_child(respawnTimer);
	
func teleport(): #opcja teleportu na losowa pozycje
	if teleTimer.get_time_left() > 0:
		return;
	speedVector = Vector2(0.0, 0.0);
	ship.set_pos(get_random_pos());
	samplePlayer.play("tele", false);
	up_key_hold = false;
	teleTimer.start();
	
func get_random_pos():
	randomize();
	return Vector2(rand_range(0, lib.WIDTH), rand_range(0, lib.HEIGHT));
func connect_events(): #podpiecie eventow
	self.connect("fire_anim", self, "launchAnimatedObject");
	self.connect("fire_bullet", self, "ufo_fire");
func ufo_fire(position, rotation, speedVector, owner): #obsluga eventu strzalu ufo
	var bullet = _spawner.new_bullet(position, rotation, speedVector, owner);
	add_child(bullet);
	bullet.set_modulate(Color(0.2, 1.0, 0.1, 1.0));
	
extends Node2D

const DEFAULT_HIGHSCORE = [["PL5", "100"], ["PL4", "100"], ["PL3", "100"], ["PL2", "100"], ["PL1", "100"]]
const HIGHSCORE_FILE = "user://hs.dat"
const INPUT_TIME = 0.1;
const ROW_SIZE = 30;
var chars = ["_","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0","-"]
var inputTimer;
var letterPos = 0;
var iLetters = [0, 0, 0];
var highscore = false;
var score;
var scores = [];
var blink_shader;

func _enter_tree():
	blink_shader = get_node("highScore/letter1").get_material();
	readScores();
	inputTimer = Timer.new();
	inputTimer.set_autostart(false);
	inputTimer.set_wait_time(INPUT_TIME);
	inputTimer.set_one_shot(true);
	add_child(inputTimer);
	score = get_node("/root/globals").score;
	if score == null:
		score = 0;
	highscore = hasHighScore();
	if highscore:
		get_node("highScore").show();
		get_node("highscoreList").hide();
	else:
		showHighscore();
	set_process(true);
func checkInput():
	if Input.is_action_pressed("ui_down") && inputTimer.get_time_left() == 0:
		inputTimer.start();
		iLetters[letterPos] += 1;
		if iLetters[letterPos] == chars.size():
			iLetters[letterPos] = 0;
			
	if Input.is_action_pressed("ui_up") && inputTimer.get_time_left() ==  0:
		inputTimer.start();
		iLetters[letterPos] -= 1;
		if iLetters[letterPos] == - 1:
			iLetters[letterPos] = chars.size() - 1;
			
	if Input.is_action_pressed("ui_right") && inputTimer.get_time_left() ==  0:
		inputTimer.start();
		if letterPos < 2:
			letterPos += 1;
			set_blink();
			
	if Input.is_action_pressed("ui_left") && inputTimer.get_time_left() ==  0:
		inputTimer.start();
		if letterPos > 0:
			letterPos -= 1;
			set_blink();
	if Input.is_action_pressed("ui_accept"):
		writeScore();
		highscore = false;
		showHighscore();
	get_node("/root/globals").score = -1;
	get_node("highScore/letter1").set_text(chars[iLetters[0]]);
	get_node("highScore/letter2").set_text(chars[iLetters[1]]);
	get_node("highScore/letter3").set_text(chars[iLetters[2]]);
func set_blink():
	get_node("highScore/letter1").set_material(null);
	get_node("highScore/letter2").set_material(null);
	get_node("highScore/letter3").set_material(null);
	var node = get_node("highScore/letter" + str(letterPos + 1)).set_material(blink_shader);
func _process(delta):
	if highscore:
		checkInput();
	else:
		if Input.is_action_pressed("ui_exit"):
			get_node("/root/globals").setScene("res://main.xscn");
func showHighscore():
	get_node("highScore").hide();
	get_node("highscoreList").show();
	for i in range(0, scores.size()):
		var hName = scores[i][0];
		var hScore = str(scores[i][1]);
		var text = str(hName);
		text += ".................................";
		text += str(hScore);
		get_node("highscoreList/hs" + str(scores.size() - i)).set_text(text);
func getName():
	var name = chars[iLetters[0]];
	name += chars[iLetters[1]];
	name += chars[iLetters[2]];
	return name;
func hasHighScore():
	return scores[0][1] < score;
func readScores():
	var f = File.new();
	var strScores;
	if !f.file_exists(HIGHSCORE_FILE):
		strScores = DEFAULT_HIGHSCORE;
	else:
		f.open_encrypted_with_pass(HIGHSCORE_FILE, File.READ,  "lubiekebab");
		strScores =  f.get_var();
		f.close();

	for line in strScores:
		scores.append([line[0], int(line[1])]);
func writeScore():
	var previousScore = null; #poprzedni wpis jezeli dodany zostal wynik do listy
	for idx in range(0, scores.size()): #lecimy od najwyzszego wyniku
		var i = scores.size() - idx - 1;
		if previousScore != null: #jezeli wypelniony jest previousScore to znaczy ze poprzedni wpis zostal przesuniety w dol
			var currentScore = [scores[i][0], scores[i][1]];
			scores[i] = previousScore; #podmienianie aktualnego na poprzedni
			previousScore = currentScore;
			continue;
		if scores[i][1] < score: #jezeli aktualny wynik jest mniejszy od wpisanego podmieniamy i przepisujemy tabele w dol
			previousScore = [scores[i][0], scores[i][1]]; #nie moze byc scores[i] bo przekazuje referencje i przy zmianie w scores podaje bledny wynik tutaj
			scores[i][0] = getName();
			scores[i][1] = score;
	var f = File.new();
	f.open_encrypted_with_pass(HIGHSCORE_FILE, File.WRITE, "lubiekebab");
	f.store_var(scores);
	f.close();
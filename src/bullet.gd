extends "res://classes.gd".GameObject

const LIB = preload("res://classes.gd");

var startPos; #chyba juz nie potrzebne
var lifetime; #ile pocisk zyje
var traveled; #ile juz przelecial
var owner; #czyj jest pocisk

#konstruktor, potrzebna nazwa dla instancji, pozycja, obrot i predkosc statku 
func init(name, position, rotation, speedVector, owner):
	.init(name, position, speedVector);
	
	self.speedVector += Vector2(sin(rotation), cos(rotation)).normalized() * LIB.BULLET_SPEED * -1;
	if self.speedVector.length() < LIB.BULLET_SPEED: #a tu zabezpieczenie (troche niefizyczne, ale tak bylo w oryginale), jezeli strzelamy do tylu to pocisku nie stoja w miejscu, tylko maja minimalna predkosc
		self.speedVector -= speedVector;

	add_to_group(LIB.GRP_MOVING);
	add_to_group(LIB.GRP_BULLETS);
	add_to_group(LIB.GRP_COLLISIONS);
	self.owner = owner;
	self.animName = "DESTROY_BULLET";
	self.set_rot(rotation);
	lifetime = 0;
	traveled = 0;
	startPos = position;
	self.set_z(LIB.Z_BULLET);

func move(delta):
	.move(delta);
	lifetime += delta;
	traveled += abs(speedVector.length() * delta);
	if !isAlive():
		destroy();
		
func isAlive(): #sprawdzanie czy zywy
	return traveled < LIB.BULLET_TRAVEL;
func destroy():
	fire_anim("DESTROY_BULLET");
	.destroy();
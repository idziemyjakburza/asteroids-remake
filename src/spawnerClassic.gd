
extends Node2D
const SCRIPT_ROCK = preload("res://src/rock.gd");
const SCRIPT_BULLET = preload("res://src/bullet.gd");
const SCRIPT_UFO = preload("res://src/ufo.gd");


const ROCK_PATH = {
	0 : "large",
	1 : "medium",
	2 : "small"
}

var rockCounter = 0;
var bulletCounter = 0;
var ufoCounter = 0;

func _ready():
	var rock = new_rock(0);
	add_child(rock);
	
func get_player():
	return get_node("ship").duplicate();
func get_background():
	return get_node("background").duplicate();
	
func new_bullet(position, rotation, speedVector, owner):
	var bullet = get_node("bullet").duplicate();
	bullet.set_script(SCRIPT_BULLET);
	bullet.init("bullet" + str(bulletCounter), position, rotation, speedVector, owner);
	bulletCounter += 1;
	return bullet;
	
func new_rock(size):
	var sizeStr = str(ROCK_PATH[size]);
	var idx = rand_int(1, get_node("rocks/" + sizeStr).get_child_count());
	var rock = get_node("rocks/" + sizeStr + "/rock" + str(idx)).duplicate();
	rock.set_script(SCRIPT_ROCK);
	rock.init("rock_" + sizeStr + str(rockCounter), size);
	rockCounter += 1;
	return rock;

func new_ufo(isLarge):
	var ufo;
	if isLarge:
		ufo = get_node("ufoL").duplicate();
	else:
		ufo = get_node("ufoS").duplicate();
	ufo.set_script(SCRIPT_UFO);
	ufo.init("ufo" + str(ufoCounter), isLarge);
	ufoCounter += 1;
	return ufo;
	
func rand_int(mn, mx):
	randomize();
	return floor(rand_range(mn, mx + 1));
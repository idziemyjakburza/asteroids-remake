
extends "res://classes.gd".GameObject

const LIB = preload("res://classes.gd");
const SIZES = [256.0, 128.0, 60.0]; #wspolczynniki rozmiaru, troche sie 2 asteroida rozjezdza jeszcze
const BASE_WIDTH = 256.0;
var size; #rozmiar 0+
var rotSpeed; #predkosc obrotu do animacji
const ANIMS = { 0:"EXPLOSION_R_BIG", 1:"EXPLOSION_M_BIG", 2:"EXPLOSION_S_BIG" }
#(pseudo)konstruktor, tu bez filozofii
func init(name, size):
	.init(name, Vector2(), Vector2()); 
	random();
	self.size = size;
	self.animName = "DESTROY_ROCK";
	
	#randomowa predkosc i obrot
	self.rotSpeed = .randomRange(-1 * LIB.ROCK_MAX_ROT, LIB.ROCK_MAX_ROT);
	#ustawianie wielkosci skalek na razie zakladamy żze bazowa tekstura ma BASE_WIDTH, potem do zmiany pewnie kazda osobna
	add_to_group(LIB.GRP_MOVING); #do grupy poruszajacych sie obiektow
	add_to_group(LIB.GRP_ROCKS); #i do grupy skalek
	add_to_group(LIB.GRP_PREDATOR); #i jeszcze do morderczych przedmiotow
	add_to_group(LIB.GRP_COLLISIONS);
	set_modulate(get_random_color());
	self.set_z(LIB.Z_ROCK);

func get_random_color():
	randomize();
	var r = 1 - randf() * 0.1;
	var g = 1 - randf() * 0.1;
	var b = 1 - randf() * 0.1;
	return Color(r,g,b);
#to czesc wspolna razem z predkoscia obiektu dla pociskow i skalek
func move(delta): #zrobie klase nadrzedna, wrzuce do osobnej grupy przy tworzeniu, wtedy iterujac po kazdym elemencie (niezaleznie czy skala czy pocisk) bedzie przeliczenie nowej pozycji	
	.move(delta);
	set_rot(get_rot() + rotSpeed * delta);
func get_width():
	return SIZES[size];
func score():
	if size == 0:
		return LIB.ROCK_LARGE_SCORE;
	if size == 1:
		return LIB.ROCK_MEDIUM_SCORE;
	if size > 1:
		return LIB.ROCK_SMALL_SCORE;
func size():
	return size;
func random():
	set_pos(get_random_pos());
	speedVector = get_random_speed(1);
func get_random_pos():
	return Vector2(.randomRange(0, LIB.WIDTH), .randomRange(0, LIB.HEIGHT));
func get_random_speed(level):
	var x = .randomRange(-1.0, 1.0);
	var y = .randomRange(-1.0, 1.0);
	var v = Vector2(x, y).normalized() * LIB.ROCK_MAX_AXIS_SPEED;# TODO * (level * DIFFICULTY_SCALING);
	return v; 
func destroy():
	get_parent().emit_signal("fire_anim", get_pos(), speedVector, get_rot(), ANIMS[size], true);
	.destroy();

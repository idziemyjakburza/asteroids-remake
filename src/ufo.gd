
extends "res://classes.gd".GameObject

const LIB = preload("res://classes.gd");

var shootingTimer;
var directionTimer;
var lifeTimer;
var largeFactor;
var score;
var ANIMS = {true:"EXPLOSION_UFO_BIG", false:"EXPLOSION_UFO_SMALL"}

func init(name, isLarge):
	.init(name, Vector2(0,0), Vector2(0,0));
	self.animName = "DESTROY_UFO";
	if isLarge:
		largeFactor = 1.3;
		score = LIB.UFO_SMALL_SCORE;
	else:
		largeFactor = 1.0;
		score = LIB.UFO_LARGE_SCORE;
	add_to_group(LIB.GRP_PREDATOR);
	add_to_group(LIB.GRP_MOVING);
	add_to_group(LIB.GRP_UFO);
	add_to_group(LIB.GRP_COLLISIONS);
	initTimers();
	changeDirection();
	self.set_z(LIB.Z_UFO);
	
func initTimers():
	shootingTimer = Timer.new();
	add_child(shootingTimer);
	randomize();
	shootingTimer.set_wait_time(LIB.UFO_SHOOTING_CD / largeFactor + rand_range(0, LIB.UFO_SHOOTING_RAND));
	shootingTimer.set_one_shot(false);
	shootingTimer.connect("timeout", self, "fire");
	
	directionTimer = Timer.new();
	add_child(directionTimer);
	directionTimer.set_wait_time(LIB.UFO_MOVEMENT_CD / largeFactor);
	directionTimer.set_one_shot(false);
	directionTimer.connect("timeout", self, "changeDirection");
	
	lifeTimer = Timer.new();
	add_child(lifeTimer);
	lifeTimer.set_wait_time(LIB.UFO_LIFE_TIME);
	lifeTimer.set_one_shot(true);

	shootingTimer.start();
	directionTimer.start();
	lifeTimer.start();
	
func move(delta):
	if lifeTimer.get_time_left() <= 0:
		if get_pos().x > LIB.WIDTH || get_pos().x < 0 || get_pos().y < 0 || get_pos().y > LIB.HEIGHT:
			.destroy();
			return;
		else:
			.move(delta)
	else:
		.move(delta);
func fire():
	var randomDirection = Vector2(.randomRange(-1.0, 1.0), .randomRange(-1.0,1.0)).normalized() * LIB.BULLET_SPEED * largeFactor;
	get_parent().emit_signal("fire_bullet", get_pos(), randomDirection.atan2(), randomDirection, self.get_name());
func changeDirection():
	speedVector = (speedVector.normalized() + Vector2(.randomRange(-1.0, 1.0), .randomRange(-1.0, 1.0)).normalized()).normalized() * LIB.UFO_MAX_SPEED * largeFactor;
func score():
	return score;
func get_width():
	if largeFactor > 1:
		return 128.0;
	else:
		return 64.0;
func destroy():
	get_parent().emit_signal("fire_anim", get_pos(), speedVector, get_rot(), ANIMS[largeFactor > 1], true);
	.destroy();

extends Camera2D

const LIB = preload("res://classes.gd");
var shaking = false;
var shaker;
var shakes = 0;
var value = 1;

func _init():
	shaker = Timer.new();
	shaker.set_wait_time(LIB.CMR_SHAKE_INTERVAL);
	shaker.set_autostart(false);
	shaker.set_one_shot(false);
	add_child(shaker);
	shaker.connect("timeout", self, "shake");
	
	
func start_shaking(value = 1):
#	if shaking: #nie trzesiemy juz trzesaca sie kamera
#		return;
	self.value = value;
	shakes = 0;
	shaking = true;
	shaker.start();
	
func shake():
	if !shaking:
		shaker.stop();
		set_pos(LIB.START_POS);
		return;
	randomize();
	var x = rand_range(0, value * LIB.CMR_SHAKE_DIST * 2.0);
	randomize();
	var y = rand_range(0, value * LIB.CMR_SHAKE_DIST * 2.0);
	
	var new_pos = LIB.START_POS + Vector2(x, y);
	set_pos(new_pos);
	
	shakes += 1;
	if shakes > LIB.CMR_SHAKES:
		shaking = false;
	

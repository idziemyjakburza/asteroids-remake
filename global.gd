extends Node

const CONF_PATH = "res://config/settings.cfg";
var currentScene;

var score;
var gameOver;

func create_plane(point_a, point_b):
	var dvec = (point_b - point_a).normalized();
	var normal = Vector2(dvec.y,-dvec.x);
	
	

func _ready():
	currentScene = get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1); #ustawia wybrana scene podstawowa przy uruchamianiu
	
func setScene(scene): #metoda do zmiany sceny
	currentScene.queue_free();
	currentScene = ResourceLoader.load(scene).instance();
	get_tree().get_root().add_child(currentScene);
func changeSceneTimeout(scene, time): #metoda do zmiany sceny po X sekundach
	var timer = TimedSceneChanger.new(scene, time);
	add_child_dg(self, timer);
	timer.start();
	
func readSettings():
	var file = File.new();
	if !file.file_exists(CONF_PATH):
		print("Nie udałlo sie wczytac konfiguracji");
		file.unreference();
		return;
	file.open(CONF_PATH, File.READ);
	while !file.eof_reached():
		var line = file.get_csv_line();
		Globals.set(line.get(0), line.get(1));
	file.close();
	file.unreference();
func add_child_dg(this, child):
	if child.get_parent() != null:
		return
	this.add_child(child);
#a tu klasa ktora realizuje zmiane sceny po X sekundach
class TimedSceneChanger extends Timer:
	var scene; #sciezka do scn
	func _init(scene,time): #konstruktor, inicjuje obiekt timer, ktory w momencie _timetout() zmieni scene
		self.scene = scene;
		self.set_wait_time(time);
		self.set_one_shot(true);
		self.connect("timeout", self, "_timeout")
		
	func _timeout(): #callback po uplywie zalozonego time
		get_node("/root/globals").setScene(scene);
		queue_free(); #i oczywiscie kamikadze uwolni mnostwo pamieci

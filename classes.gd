
extends Node

###############
#stale (pozniej postaram sie je przeniesc do jakiegos pliku z konfiguracja)
###############

#pole gry
const WIDTH = 1920;
const HEIGHT = 1080;
const BG_WIDTH = 256;
const BG_HEIGHT = 256;

#glowne zasady
const START_POS = Vector2(WIDTH / 2.0, HEIGHT / 2.0); #pozycja startowa
const START_LIVES = 3;
const RESPAWN_TIME = 2;
const SCENE_CHANGE_TIME = 5;
#stale dla poruszania statkiem
const SPEED_FACTOR = 1.0; #wspolczynnik predkosci statku
const ACC_FACTOR = 5.6; #wspolczynnik przyspieszania statku
const ROTATION_SPEED = 1.0/(14.0); #wspolczynnik szybkosci obrotu
const MAX_SPEED = 300.0; #maksymalna predkosc
const SHIP_WIDTH = 32.0;
const TELE_COOLDOWN = 3;

#stale dla pociskow
const BULLET_SPEED = 600.0; #predkosc pocisku
const BULLET_LIFETIME = 0.7; #maks czas zycia pocisku
const BULLET_TRAVEL = 1200.0; #maks odleglosc po jakiej pocisk zniknie
const BULLET_AMMOUNT = 4; #maks ilosc pociskow jednoczesnych na planszy, na razie dobralem dwie powyzsze wartosci tak, 
const SHOOTING_PERIOD = 0.2; #co ile wystrzelic mozna pocisk, taki "czas na przeladowanie"
const MAX_BULLETS = 4; #hmm chyba to samo co BULLET_AMMOUNT, potem zweryfikuje

#stale dla kamieni, ograniczaja zakres random() od -x do x 
const ROCK_MAX_AXIS_SPEED = 100.0; #maksymalna szybkosc wzgledem osi (maksymalna predkosc = this * sqrt(2))
const ROCK_MAX_ROT = 2.0; # maksymalna predkosc animacji obrotu
const ROCK_CHILDREN = 2; #ile dzieciaczków martwa skałlka urodzi
const ROCKS_LARGE = "rockL"; #nazwy elementów grupująacych kolejne rozmiary skałl
const ROCKS_MED = "rockM";
const ROCKS_SMALL = "rockS";

#ufo
const UFO_SHOOTING_CD = 3;
const UFO_SHOOTING_RAND = 0.5;
const UFO_MOVEMENT_CD = 2;
const UFO_MAX_SPEED = 100.0;
const UFO_MIN_SCORE = 1000;
const UFO_LIFE_TIME = 10;
const UFO_SPAWN_DELAY = 20#10; #minimalne odstepy po jakim wleci ufo
const UFO_SPAWN_LEVEL = 10#30; #a ten czas bedzie sie skracal


#score
const ROCK_LARGE_SCORE = 20;
const ROCK_MEDIUM_SCORE = 50;
const ROCK_SMALL_SCORE = 100;
const UFO_SMALL_SCORE = 1000;
const UFO_LARGE_SCORE = 200;

const DIFFICULTY_SCALING = 10; #o ile beda sie szybciej poruszac asteroidy na kazdy poziom;
#grupy
const GRP_SHIP = "ship";
const GRP_BULLETS = "bullets";
const GRP_ROCKS = "rocks";
const GRP_GHOSTS = "ghostShips";
const GRP_MOVING = "moving";
const GRP_PREDATOR = "ikill";
const GRP_UFO = "ufoki";
const GRP_ANIM = "anim";
const GRP_COLLISIONS = "collisions";

#animacje
const ANIM_ROCK_DESTROY = "ROCK_DESTROY";
const ANIM_DESTROY_TIME = 2.0;

#dzwieki
const MAX_VOICES = 30;

#efekty kamery
const CMR_SHAKE_INTERVAL = 0.04;
const CMR_SHAKES = 3;
const CMR_SHAKE_DIST = 50 / sqrt(2.0);


#UI
const Z_BG = -1000;
const Z_ROCK = 100;
const Z_UFO = 200;
const Z_SCORE = 201
const Z_SHIP = 400;
const Z_BULLET = 199;
const Z_ANIM = 900;
const Z_UI = 1000;


const DEBUG = false; #czy wlaczone info do debugu

##########################
##########################
var bulletTexture;


func setBulletTexture(texture):
	bulletTexture = texture;

#bazowa klasa dla obiektow poruszajacych sie
class GameObject extends Node2D:
	var lib;
	func _get_property_list():
		lib = get_node("/root/classes");

	var speedVector;
	var animName;
	var texture;
	var voice_id; #do dzwiekow potrzebne

	func init(name, position, speedVector):
		self.speedVector = speedVector;
		set_name(name);
		set_pos(position);

	func move(delta):
		#tele
		if get_pos().x > WIDTH:
			set_pos(get_pos() - Vector2(WIDTH, 0.0));
		elif get_pos().x < 0:
			set_pos(get_pos() + Vector2(WIDTH, 0.0));
		if get_pos().y < 0:
			set_pos(get_pos() + Vector2(0.0, HEIGHT));
		elif get_pos().y > HEIGHT:
			set_pos(get_pos() - Vector2(0.0, HEIGHT));
		#/tele
		
		set_pos(get_pos() + speedVector * delta);
	
	func destroy(): #destruktor, dobre nawyki dla wydajnosci
#		get_parent().emit_signal("fire_anim", get_pos(), speedVector, animName);
		self.clearGroups(self);
		queue_free();
	static func randomRange(start, end):
		randomize();
		return rand_range(start, end);
	static func random():
		randomize();
		return random();
	#kasujemy grupy wszystkie obiektu
	static func clearGroups(object):
		if object extends Node: #grupy zdefiniowane sa dopiero na poziomie Node
			for group in object.get_groups():
				object.remove_from_group(group);
	func fire_anim(name):
		get_parent().emit_signal("fire_anim", get_pos(), speedVector, get_rot(), name);
	
#to jest malo istotne z punktu widzenia dodawania animacji
class Anim extends GameObject:
	func _init(name, position, speedVector, rotation, animRef):
		.init(name, position, speedVector);
		self.animName = null;
		set_name(name);
		set_pos(position);
		set_rot(rotation);
		self.speedVector = speedVector;
		var anim = animRef.duplicate();
		add_child(anim);
		add_to_group(GRP_ANIM);
		add_to_group(GRP_MOVING);
		play();
		set_z(Z_ANIM);
	func move(delta):
		var anim = get_child(0);
		if anim extends Particles2D && !anim.is_emitting() || anim extends Sprite && !anim.get_child(0).is_playing():
			.destroy();
			return;
		.move(delta);
	func play():
		get_child(0).get_child(0).play("default");
class AnimParticles extends Anim:
	func _init(name, position, speedVector, rotation, animRef).(name, position, speedVector, rotation, animRef):
		pass
	func play():
		var anim = get_child(0);
		for child in anim.get_children():
			if child extends Particles2D:
				child.set_emitting(true);
		anim.set_emitting(true);